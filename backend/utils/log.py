import logging


def getlogger(name):
    """ Utility method for class specific logging
    """
    logger = logging.getLogger(name)
    if not logger.handlers:
        logger.propagate = False
        logger.setLevel(logging.INFO)
        console = logging.StreamHandler()
        formatter_console = logging.Formatter(
            '%(asctime)s %(levelname) -10s %(name) -10s'
            ' %(funcName) -10s %(lineno) -5d  %(message)s'
        )
        console.setFormatter(formatter_console)
        logger.addHandler(console)
    return logger
