from flask import Flask
from flask_cors import CORS
from flask_restful import Api
from backend.config import DevelopmentConfig
from backend import resources as res
from backend.db import db
from flask_login import LoginManager

app = Flask(__name__)
app.config.from_object(DevelopmentConfig)


# allow cors
CORS(app)

# add api
api = Api(app)

# Setup the app with the config.py file
app.config.from_object('backend.config')

# add resources

api.add_resource(res.HelloResource, '/api/hello')
api.add_resource(res.StudentsResource, '/api/students')

# add database
db.init_app(app)

# add login
login_manager = LoginManager(app)
login_manager.init_app(app)


# @login_manager.user_loader
# def user_loader(user_id):
#     """Given *user_id*, return the associated User object.
#
#     :param unicode user_id: user_id (email) user to retrieve
#
#     """
#     try:
#         return UserModel.query.get(user_id)
#     except:
#         return None


if __name__ == "__main__":
    app.run()
