from backend.db import db


class StudentsModel(db.Model):
    __tablename__ = "students"
    id = db.Column(db.Integer, primary_key=True)
    nume = db.Column(db.String)
    nota = db.Column(db.Integer)

    def __init__(self, nume, nota):
        self.nume = nume
        self.nota = nota

    def __str__(self):
        return self.nume

    def as_dict(self):
        """Returns info as dict"""
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
