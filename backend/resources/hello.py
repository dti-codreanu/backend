from flask_restful import Resource
from flask_login import login_required


class HelloResource(Resource):

    def get(self):
        return {"message": "Hello, World version 1.0.6"}
