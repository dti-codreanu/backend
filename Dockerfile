# use the python 3.7.5 image
FROM python:3.7.5-stretch

# set working directory app/
WORKDIR /app

#coppy this dirrectory contents into the container at /app
ADD . /app

# install the dependencies
RUN pip install -r requirements.txt
RUN pip install -e .
RUN alembic upgrade head

# run the command to start uWSGI
CMD ["uwsgi", "backend.ini"]
