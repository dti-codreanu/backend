"""create students table

Revision ID: 7c8168ef6e43
Revises:
Create Date: 2020-05-21 19:45:33.407087

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7c8168ef6e43'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "students",
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column("nume", sa.String(length=40)),
        sa.Column('nota', sa.Integer),
    )


def downgrade():
    op.drop_table("students")
