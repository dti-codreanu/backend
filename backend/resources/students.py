from flask_restful import Resource
from flask import jsonify, request
from backend.db import db
from backend import models as m
import json


class StudentsResource(Resource):

    @staticmethod
    def get():
        print('getting grades')
        students_list = []
        try:
            students_data = db.session.query(m.StudentsModel).all()
            print(students_data)
        except Exception as e:
            response = jsonify({"message": f"FATAL ERROR {e}"})
            response.status_code = 500
            return response
        for student in students_data:
            students_list.append(student.as_dict())
        print(students_list)
        response = jsonify(students_list)
        response.status_code = 200
        return response

    @staticmethod
    def delete():
        student_id = request.args.get("id", None)
        if not student_id:
            resp = jsonify({"error": "need student id"})
            resp.code = 404
        try:
            db.session.query(m.StudentsModel).filter_by(id=student_id).delete()
            db.session.commit()
            response = jsonify({"message": "student deleted"})
            response.status_code = 200
        except Exception as e:
            response = jsonify({"message": f"FATAL ERROR {e}"})
            response.status_code = 500
        return response

    @staticmethod
    def put():
        student_data = json.loads(request.data)
        try:
            print("adding new grade")
            new_grade = m.StudentsModel(
                nume=student_data["nume"],
                nota=int(student_data["nota"]),
            )
            print("set values")
            db.session.add(new_grade)
            db.session.commit()
            db.session.flush()
            response = jsonify({"message": "student grade added"})
            response.status_code = 200
        except Exception as e:
            print(e)
            response = jsonify({"message": f"FATAL ERROR {e}"})
            response.status_code = 500
        return response

    @staticmethod
    def post():
        student_data = json.loads(request.data)
        try:
            grade_data = db.session.query(m.StudentsModel).filter_by(id=student_data["id"]).one()
            grade_data.nume = student_data["nume"]
            grade_data.nota = int(student_data["nota"])
            db.session.commit()
            db.session.flush()
            response = jsonify({"message": "student grade modified"})
            response.status_code = 200
        except Exception as e:
            response = jsonify({"message": f"FATAL ERROR {e}"})
            response.status_code = 500
        return response
