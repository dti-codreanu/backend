## build docker

```
docker build -t codreanu:latest .
Successfully built 52f35b4fe721
docker run -d -p 4000:8080 --name bcodreanu codreanu:latest

docker ps
docker logs -f 5939af77c5b7

sudo iptables -nL --line-numbers
sudo iptables -A INPUT -i docker0 -j ACCEPT

```

## create database

```
sudo -i -u postgres
createuser codreanu_user
createdb codreanu_db
psql

alter user codreanu_user with encrypted password 'VEOBC0Haz8yfkhQlrP9vcWQijvdDuBgj3h';
grant all privileges on database codreanu_db to codreanu_user;
```

## create new migration

```
alembic revision -m "create students table"
```
