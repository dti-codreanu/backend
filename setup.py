from setuptools import setup

setup(
    name='backend',
    version='1.0.6',
    description='backend ',
    author='Dan Timofte',
    author_email='timoftedan@gmail.com',
    url='codreanu.qal.io',
    packages=['backend'],
    install_requires=[
        "flask",
        "flask_cors",
        "flask_restful",
        "flask_sqlalchemy",
        "sqlalchemy_searchable",
        "psycopg2-binary",
        "alembic",
        "flask_login",
        "requests",
        "uwsgi"
    ],
)
