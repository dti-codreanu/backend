from pathlib import Path
import os


class Config:
    __ROOT_PATH = Path(__file__).resolve().parent
    DEBUG = False
    TESTING = False
    # import os;os.urandom(24)
    SECRET_KEY = b'\xe0\x82\xeb\x98\x8bLL\xc3O\rv\xe5\xea\xa5\x03x\xe17 \xaa\xbcD\x9f#'
    ROOT_PATH = __ROOT_PATH
    # SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE')
    SQLALCHEMY_DATABASE_URI = "postgresql://codreanu_user:VEOBC0Haz8yfkhQlrP9vcWQijvdDuBgj3h@localhost:5432/codreanu_db"


class ProductionConfig(Config):
    DATABASE_URI = ''  # MySQL, PostgreSQL, MariaDB, etc.


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_RECORD_QUERIES = True
    ENV = 'development'


class TestingConfig(Config):
    TESTING = True
